<?php

use yii\db\Migration;

/**
 * Class m200326_061528_insert_user_data
 */
class m200326_061528_insert_user_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $users = $this->readJson('users.json');

        foreach ($users as $user) {
            $this->insert('user', [
                "id"            => $user['id'],
                "first_name"    => $user['first_name'],
                "last_name"     => $user['last_name'],
                "email"         => $user['email'],
                "personal_code" => $user['personal_code'],
                "phone"         => $user['phone'],
                "active"        => $user['active'],
                "dead"          => $user['dead'],
                "lang"          => $user['lang'],
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $users = $this->readJson('users.json');

        foreach ($users as $user) {
            $this->delete('user', ['id' => $user['id']]);
        }
    }

    /**
     * @param $file string
     * @return mixed array
     */
    private function readJson($file)
    {
        $url = Yii::$app->basePath . '/' . $file;
        return json_decode(
            file_get_contents($url),
            true
        );
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200326_061528_insert_user_data cannot be reverted.\n";

        return false;
    }
    */
}
