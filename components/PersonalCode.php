<?php

namespace app\components;

class PersonalCode
{
    /**
     * Get Birthday by personal code
     *
     * @param $personalCode
     * @return \Datetime
     * @throws \Exception
     */
    static function getBirthday($personalCode)
    {
        $g = substr($personalCode, 0, 1);
        $year = intval(substr($personalCode, 1, 2));
        $month = substr($personalCode, 3, 2);
        $day = substr($personalCode, 5, 2);

        if ($g <= 2) {
            $year += 1800;
        } elseif ($g <= 4) {
            $year += 1900;
        } elseif ($g <= 6) {
            $year += 2000;
        }

        return new \Datetime($year . '-' . $month . '-' . $day);
    }

    /**
     * Get Age by personal code
     *
     * @param $personalCode
     * @return \DateInterval|false
     * @throws \Exception
     */
    static function getAge($personalCode)
    {
        return self::getBirthday($personalCode)->diff(new \Datetime());
    }

    /**
     * Get Underage status by personal code
     *
     * @param $personalCode
     * @param int $legalAge
     * @return bool
     * @throws \Exception
     */
    static function isUnderage($personalCode, $legalAge = 18)
    {
        return (self::getAge($personalCode)->y < $legalAge);
    }
}