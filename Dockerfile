FROM yiisoftware/yii2-php:7.2-apache

WORKDIR /app
COPY . .

RUN composer install --no-plugins --no-scripts --no-autoloader --prefer-dist --no-dev

RUN chmod 755 ./start.sh
RUN cp ./start.sh /start.sh

CMD ["/start.sh"]