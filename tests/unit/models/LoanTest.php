<?php


namespace tests\models;

use app\components\PersonalCode;
use Codeception\Test\Unit;


class LoanTest extends Unit
{
    const EIGHTEEN_YEARS = 6575;

    /**
     * @dataProvider userPersonalCode
     *
     * @param string $personalCode
     * @param integer $age
     * @throws \Exception
     */
    public function testUserAge($personalCode, $age)
    {
        $this->assertEquals($age, PersonalCode::getAge($personalCode)->y);
        $this->assertEquals($age < 18, PersonalCode::isUnderage($personalCode));
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function userPersonalCode()
    {
        $dataTest = [];

        for ($i = 0; $i < 20; $i++) {
            $dataTest[] = $this->personalCodeGenerator($i < 10);
        }

        return $dataTest;
    }


    private function personalCodeGenerator($underAge = false)
    {
        if (!$underAge) {
            $randomDays = rand(366, 6209); // Min 1 years - max 17 years
        } else {
            $randomDays = rand(6575, 29219); // Min 18 years - max 80 years
        }

        $birthday = new \DateTime();
        $birthday->modify('-' . $randomDays . ' days');

        $birthdayString = $birthday->format('ymd');
        if (strlen($birthdayString) == 5) {
            $birthdayString = '0' . $birthdayString;
        }
        if ($birthday->format('Y') <= 1900) {
            $g = rand(1, 2);
        } elseif ($birthday->format('Y') <= 2000) {
            $g = rand(3, 4);
        } elseif ($birthday->format('Y') <= 2100) {
            $g = rand(5, 6);
        } else {
            $g = 0; // not found
        }

        return [
            $g . $birthdayString . '5465', // Personal code
            $birthday->diff(new \DateTime())->y // Age
        ];
    }
}