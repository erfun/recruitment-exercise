<?php
$db = require __DIR__ . '/db.php';
// test database! Important not to run tests on production or development databases
$db['dsn'] = 'pgsql:host=' . env("POSTGRES_HOST", 'db_test') . ';dbname=' . env('POSTGRES_DB_TEST', 'example');

return $db;
