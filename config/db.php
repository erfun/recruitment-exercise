<?php

return [
    'class'    => \yii\db\Connection::class,
    'dsn'      => 'pgsql:host=' . env("POSTGRES_HOST", 'db') . ';dbname=' . env('POSTGRES_DB', 'example'),
    'username' => env('POSTGRES_USER', 'example'),
    'password' => env('POSTGRES_PASSWORD', 'example'),
    'charset'  => 'utf8'
];