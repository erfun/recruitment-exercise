<?php

namespace app\validators;

use yii\validators\Validator;

class EstonianPersonalCodeValidator extends Validator
{
    public function validateAttribute($model, $attribute)
    {
        if (!preg_match('/^[1-6][0-9]{2}[0-1][0-9][0-9]{2}[0-9]{4}$/', $model->$attribute)) {
            $this->addError($model, $attribute, 'The personal code is invalid.');
        }
    }
}