<?php

namespace app\models;

use app\components\PersonalCode;
use yii\db\ActiveRecord;


class Loan extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'loan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['user_id', 'amount', 'interest', 'duration', 'start_date', 'end_date', 'campaign'], 'required'],
            [['user_id', 'duration', 'campaign'], 'default', 'value' => null],
            [['user_id', 'duration', 'campaign'], 'integer'],
            [['amount', 'interest'], 'number'],
            [['start_date', 'end_date'], 'date', 'format' => 'Y-m-d'],
            [['status'], 'boolean'],
            ['user_id', function ($attribute, $params, $validator) {
                if ($this->user->age()->y < 18) {
                    $this->addError($attribute, 'Underage.');
                }
            }]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'user_id'    => 'User ID',
            'amount'     => 'Amount',
            'interest'   => 'Interest',
            'duration'   => 'Duration',
            'start_date' => 'Start Date',
            'end_date'   => 'End Date',
            'campaign'   => 'Campaign',
            'status'     => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @param int $legalAge
     * @return bool
     * @throws \Exception
     */
    public function isUnderage($legalAge = 18)
    {
        return PersonalCode::isUnderage($this->user->personal_code, $legalAge);
    }
}