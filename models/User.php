<?php

namespace app\models;

use DateTime;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use app\components\PersonalCode;
use yii\web\NotFoundHttpException;
use app\validators\EstonianPersonalCodeValidator;

/**
 * Class User
 *
 * @package app\models
 */
class User extends ActiveRecord implements IdentityInterface
{
    public $id;
    public $username;
    public $password;
    public $authKey;
    public $accessToken;

    private static $users = [
        '100' => [
            'id'          => '100',
            'username'    => 'admin',
            'password'    => 'admin',
            'authKey'     => 'test100key',
            'accessToken' => '100-token',
        ],
        '101' => [
            'id'          => '101',
            'username'    => 'demo',
            'password'    => 'demo',
            'authKey'     => 'test101key',
            'accessToken' => '101-token',
        ],
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'active' => 'Status',
            'dead'   => 'Vital Status',
            'lang'   => 'Language',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'email', 'personal_code', 'active', 'dead', 'phone'], 'required'],
            [['first_name', 'last_name', 'lang'], 'string'],
            [['personal_code', 'phone'], 'integer'],
            [['active', 'dead'], 'boolean'],
            [['email'], 'email'],
            ['personal_code', EstonianPersonalCodeValidator::className()]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        foreach (self::$users as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }

    /**
     * @param integer $id
     * @return User|null
     * @throws NotFoundHttpException
     */
    public static function findOneOrFail($id)
    {
        $user = self::findOne($id);

        if ($user !== null) {
            return $user;
        }

        throw new NotFoundHttpException('The User Not Found.');
    }

    /**
     * Get user birthday as Datetime object
     *
     * @return Datetime user birthday
     * @throws \Exception
     */
    public function getBirthDate()
    {
        return PersonalCode::getBirthday($this->personal_code);
    }

    /**
     * Get user age as Datetime object
     *
     * @return \DateInterval|false user birthday
     * @throws \Exception
     */
    public function age()
    {
        return PersonalCode::getAge($this->personal_code);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoans()
    {
        return $this->hasMany(Loan::className(), ['user_id' => 'id']);
    }
}
