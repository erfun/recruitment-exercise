<?php


namespace app\controllers;


use yii;
use app\models\Loan;
use app\models\User;
use yii\web\Controller;
use app\models\LoanSearch;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

class LoanController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * List all Loans
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Loan::find(),
            'sort'  => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        $searchModel = new LoanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Show a single loan
     *
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Create a loan
     *
     * @param $user_id
     * @return string|yii\web\Response
     * @throws yii\web\NotFoundHttpException
     */
    public function actionCreate($user_id)
    {
        $model = new Loan();
        $user = User::findOneOrFail($user_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('message', 'Insert Successfully');
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model'   => $model,
            'user_id' => $user->getAttribute('id'),
        ]);
    }

    /**
     * Update a loan
     *
     * @param $id
     * @return string|yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('message', 'Insert Successfully');
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model'   => $model,
            'user_id' => $model->user->id,
        ]);
    }

    /**
     * Delete a loan
     *
     * @param $id
     * @return yii\web\Response
     * @throws \Throwable
     * @throws yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->getSession()->setFlash('message', 'Delete Successfully');

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return Loan|null
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Loan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The Loan could not found.');
    }
}