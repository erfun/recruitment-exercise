<?php


namespace app\controllers;


use app\models\Loan;
use Yii;
use app\models\User;
use yii\web\Controller;
use app\models\UserSearch;
use yii\data\ActiveDataProvider;

/**
 * Class UserController
 *
 * @package app\controllers
 */
class UserController extends Controller
{
    /**
     * List of all users with pagination
     *
     * @access public
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => User::find(),
            'sort'  => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel'  => $searchModel,
        ]);
    }


    /**
     * Create new User
     *
     * @access public
     * @return mixed
     */
    public function actionCreate()
    {
        $user_model = new User();

        $formData = Yii::$app->request->post();

        if ($user_model->load($formData) && $user_model->save()) {
            Yii::$app->getSession()->setFlash('message', 'Insert Successfully');
            return $this->redirect(['user/index']);
        } elseif ($user_model->load($formData)) {
            Yii::$app->getSession()->setFlash('message', 'Insert Failed');
        }

        return $this->render('create', [
            'model' => $user_model
        ]);
    }


    /**
     * View User
     *
     * @access public
     * @param integer $id
     * @return mixed
     * @throws yii\web\NotFoundHttpException
     */
    public function actionView($id)
    {
        $user_model = User::findOneOrFail($id);

        return $this->render('view', [
            'model' => $user_model
        ]);
    }


    /**
     * Update User
     *
     * @access public
     * @param integer $id
     * @return mixed
     * @throws yii\web\NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $user_model = User::findOneOrFail($id);

        if ($user_model->load(Yii::$app->request->post()) && $user_model->save()) {

            Yii::$app->getSession()->setFlash('message', 'Update Successfully');
            return $this->redirect(['user/index']);

        } elseif ($user_model->load(Yii::$app->request->post())) {

            Yii::$app->getSession()->setFlash('message', 'Insert Failed');

        }

        return $this->render('update', [
            'model' => $user_model
        ]);
    }


    /**
     * delete User
     *
     * @access public
     * @param integer $id
     * @return mixed
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $user_model = User::findOneOrFail($id);

        if (!$user_model)
            Yii::$app->getSession()->setFlash('message', 'User not found!');

        $loans = Loan::find()
            ->where(['user_id' => $id])
            ->count();

        if ($loans) {
            Yii::$app->getSession()->setFlash('message', "could not delete the user, user has $loans loan");
        } else {
            if ($user_model->delete())
                Yii::$app->getSession()->setFlash('message', 'Delete Successfully');
        }

        return $this->redirect(['user/index']);
    }

}