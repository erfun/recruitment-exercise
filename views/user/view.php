<?php

use yii\helpers\Html;

$this->title = 'View User';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['/user/index']];
$this->params['breadcrumbs'][] = $this->title;

/**
 * @var $model \app\models\User
 */
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <ul class="list-group">
        <li class="list-group-item d-flex justify-content-between align-items-center">
            Name:
            <?= $model->getAttribute('first_name') . ' ' . $model->getAttribute('last_name') ?>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center">
            Email: <?= $model->getAttribute('email') ?>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center">
            Birth Date:
            <?= $model->getBirthDate()->format('d M Y') ?>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center">
            Age:
            <?= $model->age()->y ?>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center">
            Personal Code: <?= $model->getAttribute('personal_code') ?>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center">
            Phone: <?= $model->getAttribute('phone') ?>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center">
            Status: <?= $model->getAttribute('active') ? 'Active' : 'Inactive'; ?>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center">
            Vital Status: <?= $model->getAttribute('dead') ? 'Dead' : 'Alive'; ?>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center">
            Language: <?= $model->getAttribute('lang') ?>
        </li>
    </ul>

    <div class="row">
        <div class="col-lg-2">
            <?= Html::a('Back', ['user/index'], ['class' => 'btn btn-primary']) ?>
        </div>
        <div class="col-lg-1">
            <?= Html::a('Add Loan', ['loan/create', 'user_id' => $model->getAttribute('id')], ['class' => 'btn btn-success']) ?>
        </div>
        <div class="col-lg-1">
            <?= Html::a('Edit', ['update', 'id' => $model->getAttribute('id')], ['class' => 'btn btn-default']) ?>
        </div>
        <div class="col-lg-1">
            <?= Html::a('Delete', ['delete', 'id' => $model->getAttribute('id')], ['class' => 'btn btn-danger']) ?>
        </div>
    </div>
</div>