<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
 * @var $model app\models\User
 */

?>

<div class="row">

    <?php $form = ActiveForm::begin([
        'id'          => 'user-form',
        'layout'      => 'horizontal',
        'fieldConfig' => [
            'template'     => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-7\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-2 control-label'],
        ],
    ]);
    ?>

    <?= $form->field($model, 'first_name')->textInput(['autofocus' => true]) ?>
    <?= $form->field($model, 'last_name')->textInput() ?>
    <?= $form->field($model, 'email')->input('email') ?>
    <?= $form->field($model, 'phone')->textInput() ?>
    <?= $form->field($model, 'personal_code')->textInput() ?>
    <?= $form->field($model, 'lang')->textInput() ?>
    <?= $form->field($model, 'active')->checkbox()->label('User is active.') ?>
    <?= $form->field($model, 'dead')->checkbox()->label('User is Dead.') ?>

    <div class="form-group">
        <div class="col-lg-offset-2 col-lg-1">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-success', 'name' => 'user-form']) ?>
        </div>
        <div class="col-lg-3">
            <?= Html::a('Back', Yii::$app->request->referrer ?: ['user/index'], ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>