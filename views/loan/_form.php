<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var $user_id int
 * @var $this yii\web\View
 * @var $model app\models\Loan
 * @var $form yii\widgets\ActiveForm
 */
?>

<div class="loan-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'user_id')->hiddenInput(['value' => $user_id])->label(false); ?>
    <?= $form->field($model, 'amount')->textInput() ?>
    <?= $form->field($model, 'interest')->textInput() ?>
    <?= $form->field($model, 'duration')->textInput() ?>
    <?= $form->field($model, 'start_date')->textInput()->hint('Date time: yyyy-mm-dd') ?>
    <?= $form->field($model, 'end_date')->textInput()->hint('Date time: yyyy-mm-dd') ?>
    <?= $form->field($model, 'campaign')->textInput()->hint('Campaign ID') ?>
    <?= $form->field($model, 'status')->checkbox() ?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>