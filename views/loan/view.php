<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\web\YiiAsset;

/**
 * @var $this yii\web\View
 * @var $model app\models\Loan
 */

$this->title = $full_name = $model->user->getAttribute('first_name') . ' ' . $model->user->getAttribute('last_name');
$this->params['breadcrumbs'][] = ['label' => 'Loans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
YiiAsset::register($this);
?>

<div class="loan-view">

    <?php if (Yii::$app->session->hasFlash('message')): ?>
        <div class="alert alert-dismissible alert-success">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <?= Yii::$app->session->getFlash('message'); ?>
        </div>
    <?php endif; ?>

    <h1><?= Html::a($this->title, ['user/view', 'id' => $model->user->getAttribute('id')]) ?></h1>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data'  => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method'  => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model'      => $model,
        'attributes' => [
            [
                'attribute' => 'First Name',
                'format'    => 'raw',
                'value'     => function ($model) {
                    return $model->user->first_name;
                },
            ],
            [
                'attribute' => 'Last Name',
                'format'    => 'raw',
                'value'     => function ($model) {
                    return $model->user->last_name;
                },
            ],
            [
                'attribute' => 'Phone',
                'format'    => 'raw',
                'value'     => function ($model) {
                    return $model->user->phone;
                },
            ],
            [
                'attribute' => 'User Email',
                'format'    => 'raw',
                'value'     => function ($model) {
                    return $model->user->email;
                },
            ],
            [
                'attribute' => 'User Personal Code',
                'format'    => 'raw',
                'value'     => function ($model) {
                    return $model->user->personal_code;
                },
            ],
            'amount',
            'interest',
            'duration',
            'start_date',
            'end_date',
            'campaign',
            'status:boolean',
        ],
    ]) ?>
</div>