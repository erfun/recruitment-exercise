<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $searchModel app\models\LoanSearch
 * @var $loans app\models\User
 * @var $pages yii\data\Pagination
 */

$this->title = 'Loans';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="loan-index">

    <?php if (Yii::$app->session->hasFlash('message')): ?>
        <div class="alert alert-dismissible alert-success">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <?= Yii::$app->session->getFlash('message'); ?>
        </div>
    <?php endif; ?>

    <h1><?= Html::encode($this->title) ?></h1>
    <p>List of all loans</p>

    <div class="row">
        <?php Pjax::begin(); ?>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'columns'      => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'User',
                    'format'    => 'raw',
                    'value'     => function ($model) {
                        $full_name = $model->user->first_name . ' ' . $model->user->last_name;
                        return Html::a($full_name, ['user/view', 'id' => $model->user->getAttribute('id')]);
                    },
                ],
                'amount',
                'interest',
                'duration',
                'start_date:datetime',
                'end_date:datetime',
                'campaign',
                'status:boolean',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>

        <?php Pjax::end(); ?>
    </div>
</div>