<?php

use yii\helpers\Html;

/**
 * @var $user_id int
 * @var $this yii\web\View
 * @var $model app\models\Loan
 */

$this->title = 'Update Loan: ' . $model->user->first_name . ' ' . $model->user->last_name;
$this->params['breadcrumbs'][] = ['label' => 'Loans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->user->first_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="loan-update">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model'   => $model,
        'user_id' => $user_id
    ]) ?>
</div>